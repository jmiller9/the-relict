﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IT5Object
{
    void ReceiveEvent(IT5Event ev);
    void SendEvent(IT5Event ev);
    bool IsPlayer();
    System.Guid GetGuid();
}