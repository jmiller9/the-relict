﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Dictionary<string, GameObject> contained = new Dictionary<string, GameObject>();
    public int securityLevel = 0;
    public int objectiveLevel = 0;
    private Rigidbody doorFace;
    private int occupancy = 0;
    private float speed = 5.0f;


    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform child in transform.parent) {
            if (child.gameObject.name == "DoorFace") {
                doorFace = child.gameObject.GetComponent<Rigidbody>();
                break;
            }
        }
    }
    
    void FixedUpdate() {
        if (doorFace != null) {
            
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<Humanoid>() != null) {
            if (!contained.ContainsKey(collider.gameObject.name)) {
                Debug.Log(collider.gameObject.name + " entered Door Space");
                contained.Add(collider.gameObject.name, collider.gameObject);
                occupancy++;
                doorFace.velocity = -transform.right * speed;
            }
        }
    }

    void OnTriggerExit(Collider collider) {
        if (collider.gameObject.GetComponent<Humanoid>() != null) {
            if (contained.ContainsKey(collider.gameObject.name)) {
                Debug.Log(collider.gameObject.name + " exited Door Space");
                contained.Remove(collider.gameObject.name);
                occupancy--;
                if (occupancy < 0) {
                    occupancy = 0;
                }
                if (occupancy == 0) {
                    doorFace.velocity = transform.right * speed;
                }
            }
        }
    }
}
