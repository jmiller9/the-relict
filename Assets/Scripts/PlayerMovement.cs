﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour, Humanoid
{
    public float speed = 3f;            // The speed that the player will move at.

    Vector3 movement;                   // The vector to store the direction of the player's movement.
    Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
    float camRayLength = 100f;          // The length of the ray from the camera into the scene.
    private bool firing = false;
    private bool last_firing = false;
    public GameObject bullet;
    private float bulletSpeed = 15.0f;
    int currentWeaponDamage = 50;
    System.Guid guid = System.Guid.NewGuid();
    bool lastPausedPressed = false;
    GameManager gameManager;
    private int hitFrames = 0;
    private int protectFrames = 0;  // Protects the Player from receiving damage while > 0
    private int shootFrames = 0;
    private int meleeFrames = 0;
    public int health = 100;

    void Awake ()
    {
        // Create a layer mask for the floor layer.
        floorMask = LayerMask.GetMask ("Floor");

        // Set up references.
        anim = GetComponent <Animator> ();
        playerRigidbody = GetComponent <Rigidbody> ();

        GameObject gmObj = GameObject.Find("GameManager");
        gameManager = gmObj.GetComponent<GameManager>();
        gameManager.subscribe(this);
    }

    void Start() {
        if (gameManager != null) {
            gameManager.updateHealthLabel(GetHealth());
            gameManager.updateWeaponLabel(GetWeapon());
            gameManager.updateItemLabel(GetItem());
        }
    }

    void Update() {
        bool pausePressed = Input.GetKeyDown(KeyCode.Return);
        if (!lastPausedPressed && pausePressed) {
            gameManager.togglePause();
        }
        lastPausedPressed = pausePressed;
    }
    void FixedUpdate ()
    {
        if (health <= 0) {
            return;
        }
        if (shootFrames > 0) {
            shootFrames++;
            if (shootFrames >= 5) {
                shootFrames = 0;
            }
        }
        if (meleeFrames > 0) {
            meleeFrames++;
            if (meleeFrames > 40) {
                meleeFrames = 0;
            }
        }
        float h = 0;
        float v = 0;
        if (hitFrames == 0) {
            // Store the input axes.
            h = Input.GetAxisRaw ("Horizontal");
            v = Input.GetAxisRaw ("Vertical");
            last_firing = firing;
            // TODO: Use Input.GetButton("Fire1") for full auto
            firing = Input.GetButtonDown("Fire1");
            bool meleePressed = (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl));
            if (firing) {
                if (shootFrames == 0 && meleeFrames == 0) {
                    Attack(transform.position);
                    shootFrames = 1;
                }
            }
            else if (meleePressed) {
                if (shootFrames == 0 && meleeFrames == 0) {
                    Melee();
                    meleeFrames = 1;
                }
            }
        }
        else {
            shootFrames = 0;
        }

        // Move the player around the scene.
        Move (h, v);

        // Animate the player.
        Animating (h, v);

        if (protectFrames > 0) {
            protectFrames++;
            if (protectFrames >= 60) {
                protectFrames = 0;
            }
        }
    }

    public void ReceiveEvent(IT5Event ev) {
        if (ev.GetType() == EventType.NPC_MELEE) {
            float distance = Vector3.Distance (transform.position, ev.GetPosition());
            if (distance <= ev.GetMagnitude()) {
                Debug.Log("Player hit by NPC Melee");
                //TODO: Player should stagger
            }
        }
    }
    public void SendEvent(IT5Event ev) {
        gameManager.publish(ev);
    }
    public System.Guid GetGuid() {
        return guid;
    }

    public void Move (float h, float v)
    {
        // Set the movement vector based on the axis input.
        movement.Set (h, 0f, v);
        
        // Normalise the movement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        playerRigidbody.MovePosition (transform.position + movement);
        // Turn the player in the correct direction
        if (h != 0 || v != 0) {
            playerRigidbody.MoveRotation(Quaternion.LookRotation(movement));
        }
    }

    public void Animating (float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        if (walking) {
            anim.SetFloat("InputMagnitude", 1.0f);
            anim.SetBool("IsStopLU", false);
            anim.SetBool("IsStopRU", false);
            anim.SetFloat("Vertical", 1.0f);
        }
        else {
            anim.SetFloat("InputMagnitude", 0);
            anim.SetBool("IsStopLU", true);
            anim.SetBool("IsStopRU", true);
            anim.SetFloat("Vertical", 0);
        }
        anim.SetBool("IsShoot", firing);

        if (anim.GetBool("IsHit1") == true) {
            hitFrames++;
            if (hitFrames >= 30) {
                hitFrames = 0;
                anim.SetBool("IsHit1", false);
            }
        }
        if (anim.GetBool("IsMelee") == true) {
            if (meleeFrames == 0) {
                anim.SetBool("IsMelee", false);
            }
        }
    }

    public void Melee() {
        SendEvent(new IT5Event(EventType.PLAYER_MELEE, transform.position, 1.5f));
        Debug.Log("Player performs melee attack");
        anim.SetBool("IsMelee", true);
    }
    public void Attack(Vector3 v) {
        Debug.Log("Player attacks");
        //Rigidbody bulletClone = (Rigidbody) Instantiate(bullet, transform.position, transform.rotation);
        //bulletClone.velocity = transform.forward * bulletSpeed;
        Vector3 attackVector = new Vector3(transform.position.x, transform.position.y + 1.25f, transform.position.z);
        GameObject bulletClone = Instantiate(bullet, attackVector + transform.forward, transform.rotation);
        Bullet theBullet = bulletClone.GetComponent<Bullet>();
        theBullet.damage = currentWeaponDamage;
        theBullet.friendly = true;
        Rigidbody bulletRigidBody = bulletClone.GetComponent<Rigidbody>();
        bulletRigidBody.velocity = transform.forward * bulletSpeed;
    }

    public void ReachedWaypoint(Waypoint waypoint) {
        Debug.Log("*** Reached Waypoint " + waypoint.gameObject.name);
    }
    public bool ReceiveDamage(int damage, bool friendly) {
        if (friendly) {
            return false;
        }
        if (protectFrames > 0) {
            return false;
        }
        Debug.Log("Player Received Damage");
        anim.SetBool("IsHit1", true);
        health -= damage;
        protectFrames = 1;
        gameManager.updateHealthLabel(health);
        if (health <= 0) {
            anim.SetBool("IsDead", true);
            gameManager.updateHealthLabel(0);
        }
        return true;
    }
    public void SetHealth(int health) {
        this.health = health;
    }

    public int GetHealth() {
        return health;
    }

    public string GetWeapon() {
        //TODO: Implement this
        return "M1 Carbine - 30";
    }

    public string GetItem() {
        //TODO: Implement this
        return "None";
    }

    public bool IsPlayer() {
        return true;
    }
}