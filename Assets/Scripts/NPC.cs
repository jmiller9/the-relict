﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour, Humanoid
{
    Animator anim;                      // Reference to the animator component.
    Rigidbody npcRigidbody;             // Reference to the npc's rigidbody.
    public List<string> waypoints = new List<string>();

    List<Waypoint> actualWaypoints = new List<Waypoint>();
    Waypoint currentWaypoint;
    int waypointIndex = 0;
    private NavMeshAgent agent;         // Reference to the npc's NavMeshAgent
    public int health = 100;
    public float viewDistance = 8.0f;
    float timeDead = 0;
    public int fireRate = 30;
    public int damage = 20;
    private float bulletSpeed = 15.0f;
    public GameObject bullet;
    private int currentShot = 0;
    float showCorpseSeconds = 3.0f;
    float FOVInDegrees = 90.0f;
    float FOVInRadians;
    float minCosine;
    GameObject player;
    int floorMask = ~(1 << 9);
    bool stopped = false;
    System.Guid guid = System.Guid.NewGuid();
    GameManager gameManager;

    void Awake ()
    {
        // Set up references.
        anim = GetComponent <Animator> ();
        npcRigidbody = GetComponent <Rigidbody> ();

        GameObject allWaypoints = GameObject.Find("Waypoints");
        if (allWaypoints != null) {
            foreach(Transform child in allWaypoints.transform) {
                if (waypoints.Contains(child.gameObject.name)) {
                    Waypoint waypoint = child.gameObject.GetComponent<Waypoint>();
                    if (waypoint != null) {
                        actualWaypoints.Add(waypoint);
                        Debug.Log("Found waypoint " + child.gameObject.name);
                    }
                }
            }
            if (actualWaypoints.Count > 0) {
                currentWaypoint = actualWaypoints[0];
            }
        }
        player = GameObject.Find("player");
        if (player == null) {
            player = GameObject.Find("Player");
        }

        GameObject gmObj = GameObject.Find("GameManager");
        gameManager = gmObj.GetComponent<GameManager>();
        gameManager.subscribe(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;

        FOVInRadians = FOVInDegrees*Mathf.Deg2Rad;
        minCosine = Mathf.Cos(FOVInRadians/2);
    }

    void GotoNextPoint() {
        // Returns if no points have been set up
        if (actualWaypoints.Count == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = actualWaypoints[waypointIndex].transform.position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        waypointIndex = (waypointIndex + 1) % actualWaypoints.Count;
    }

    void updateRigidBody() {
        if (stopped) {
            npcRigidbody.constraints |= RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        }
        else {
            npcRigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Choose the next destination point when the agent gets
        // close to the current one.
        if (isAlive()) {
            if (agent.isStopped != stopped) {
                agent.isStopped = stopped;
                updateRigidBody();
            }

            Debug.DrawRay(transform.position, Quaternion.Euler (0,45,0)*transform.forward*10,Color.green, viewDistance);
            Debug.DrawRay(transform.position, Quaternion.Euler (0,-45,0)*transform.forward*10,Color.green, viewDistance);
            Debug.DrawRay(transform.position, transform.forward, Color.red, viewDistance);

            // Check to see if the NPC can see the Player
            Vector3 v2 = player.transform.position - transform.position;
            float dotP = Vector3.Dot(transform.forward, v2.normalized);

            if (dotP > minCosine && dotP <= 1) {
                RaycastHit hit;
                Ray ray = new Ray(transform.position, v2);
                if (Physics.Raycast(ray, out hit, viewDistance, floorMask)) {
                    if (hit.collider.GetComponent<PlayerMovement>() != null) {
                        Debug.Log("Player is in NPC's FOV. Distance = " + hit.distance);
                        stopped = true;
                        transform.LookAt(player.transform);
                        // Shoot when appropriate
                        if (currentShot > fireRate) {
                            currentShot = 0;
                        }
                        if (currentShot == 0) {
                            Attack(v2.normalized);
                        }
                        currentShot++;
                    }
                }
                else {
                    currentShot = 0;
                }
            }
            else {
                currentShot = 0;
            }

            if (!agent.pathPending && agent.remainingDistance < 0.5f) {
                GotoNextPoint();
            }
            Animating (agent.velocity.x, agent.velocity.z);
        }
        else {
            timeDead += Time.deltaTime;
            if (timeDead >= showCorpseSeconds) {
                Destroy(transform.gameObject);
            }
        }
    }

    bool isAlive() {
        return health > 0;
    }

    public void Animating (float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool moving = h != 0f || v != 0f;

        // Tell the animator whether or not the npc is moving.
        if (moving) {
            anim.SetBool("IsAware", true);
            anim.SetFloat("Speed", agent.speed);
            //anim.SetFloat("InputMagnitude", 1.0f);
            //anim.SetBool("IsStopLU", false);
            //anim.SetBool("IsStopRU", false);
            //anim.SetFloat("Vertical", 0.5f);
        }
        else {
            anim.SetFloat("Speed", 0);
            anim.SetBool("IsAware", false);
            //anim.SetFloat("InputMagnitude", 0);
            //anim.SetBool("IsStopLU", true);
            //anim.SetBool("IsStopRU", true);
            //anim.SetFloat("Vertical", 0);
        }
    }

    public void Attack(Vector3 v) {
        Debug.Log("NPC Attacks");
        Vector3 attackVector = new Vector3(transform.position.x, transform.position.y + 1.25f, transform.position.z);
        GameObject bulletClone = Instantiate(bullet, attackVector + v, transform.rotation);
        Bullet theBullet = bulletClone.GetComponent<Bullet>();
        theBullet.damage = damage;
        theBullet.friendly = false;
        Rigidbody bulletRigidBody = bulletClone.GetComponent<Rigidbody>();
        bulletRigidBody.velocity = v * bulletSpeed;
    }

    public void ReachedWaypoint(Waypoint waypoint) {
        // Check to see if the reached waypoint is the current waypoint
        if (waypoint == currentWaypoint) {
            Debug.Log("*** [npc] Reached Waypoint " + waypoint.gameObject.name);
            // Set deltaH and deltaV
            if (waypointIndex + 1 >= waypoints.Count) {
                waypointIndex = 0;
            }
            else {
                waypointIndex++;
            }
            currentWaypoint = actualWaypoints[waypointIndex];
            //Vector3 dirVec = Vector3.Normalize(currentWaypoint.transform.position - transform.position);
            //deltaH = dirVec.x;
            //deltaV = dirVec.z;
            // TODO: Enforce waypoint behavior
        }
    }
    public bool ReceiveDamage(int damage, bool friendly) {
        if (!friendly) {
            return false;
        }
        health = health - damage;
        Debug.Log("NPC received damage. Health = " + health);
        if (health <= 0) {
            Debug.Log("NPC has died");
            //Destroy(transform.gameObject);
            stopped = true;
            agent.isStopped = true;
            anim.SetBool("IsDead", true);
            gameManager.unsubscribe(this);
        }
        return true;
    }
    public void SetHealth(int health) {
        this.health = health;
    }

    public int GetHealth() {
        return health;
    }

    public bool IsPlayer() {
        return false;
    }
    public void ReceiveEvent(IT5Event ev) {
        if (ev.GetType() == EventType.PLAYER_MELEE) {
            float distance = Vector3.Distance (transform.position, ev.GetPosition());
            if (distance <= ev.GetMagnitude()) {
                Debug.Log("NPC " + GetGuid() + " hit by Player Melee");
                //TODO: NPC should stagger/get knocked down
            }
        }
        else if (ev.GetType() == EventType.NOISE_HEARD) {
            Debug.Log("Noise Heard Event Received");
        }
        else if (ev.GetType() == EventType.PLAYER_SPOTTED) {
            Debug.Log("Player Spotted Event Received");
        }
    }
    public void SendEvent(IT5Event ev) {
        gameManager.publish(ev);
    }
    public System.Guid GetGuid() {
        return guid;
    }
}
