﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void NewGame() {
        Debug.Log("TODO: Start a new game");
    }

    public void LoadGame() {
        Debug.Log("TODO: Load an existing game");
    }

    public void LoadTestScene() {
        SceneManager.LoadScene("SampleScene");
    }
}
