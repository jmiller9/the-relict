﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EventType {
    PLAYER_SPOTTED,
    NOISE_HEARD,
    PLAYER_MELEE,
    NPC_MELEE
}

public class IT5Event {
    private readonly EventType m_type;
    private Vector3 m_position;
    private float m_magnitude;
    public IT5Event(EventType type) {
        m_type = type;
        m_magnitude = 0;
    }

    public IT5Event(EventType type, Vector3 position) {
        m_type = type;
        m_position = position;
        m_magnitude = 0;
    }

    public IT5Event(EventType type, Vector3 position, float magnitude) {
        m_type = type;
        m_position = position;
        m_magnitude = magnitude;
    }

    public EventType GetType() {
        return m_type;
    }

    public Vector3 GetPosition() {
        return m_position;
    }

    public float GetMagnitude() {
        return m_magnitude;
    }
}

public class EventBus {
    private Dictionary<System.Guid, IT5Object> monitored = new Dictionary<System.Guid, IT5Object>();
    public void publish(IT5Event ev) {
        foreach(KeyValuePair<System.Guid, IT5Object> entry in monitored) {
            entry.Value.ReceiveEvent(ev);
        }
    }

    public void subscribe(IT5Object obj) {
        monitored.Add(obj.GetGuid(), obj);
    }

    public void unsubscribe(System.Guid guid) {
        monitored.Remove(guid);
    }

    public void clear() {
        monitored.Clear();
    }
}

public class GameManager : MonoBehaviour
{
    private bool paused = false;
    private bool inDialog = false;
    private readonly EventBus eventBus = new EventBus();
    private GameObject UIMenuPanel;
    private GameObject DialogPanel;
    private GameObject UIPanel;
    private Text playerHealth;
    private Text playerWeapon;
    private Text playerItem;
    private PlayerMovement player;

    void Awake () {
        Transform canvas = transform.Find("Canvas");
        foreach(Transform child in canvas) {
            if (child.gameObject.name == "UIMenuPanel") {
                UIMenuPanel = child.gameObject;
            }
            else if (child.gameObject.name == "DialogPanel") {
                DialogPanel = child.gameObject;
            }
            else if (child.gameObject.name == "UIPanel") {
                UIPanel = child.gameObject;
                foreach(Transform uiChild in child) {
                    if (uiChild.gameObject.name == "Health") {
                        playerHealth = uiChild.gameObject.GetComponent<Text>();
                    }
                    else if (uiChild.gameObject.name == "Weapon") {
                        playerWeapon = uiChild.gameObject.GetComponent<Text>();
                    }
                    else if (uiChild.gameObject.name == "Item") {
                        playerItem = uiChild.gameObject.GetComponent<Text>();
                    }
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void publish(IT5Event ev) {
        eventBus.publish(ev);
    }

    public void subscribe(IT5Object obj) {
        eventBus.subscribe(obj);
        if (obj.IsPlayer()) {
            player = (PlayerMovement)obj;
        }
    }

    public void unsubscribe(System.Guid guid) {
        eventBus.unsubscribe(guid);
    }

    public void unsubscribe(IT5Object obj) {
        eventBus.unsubscribe(obj.GetGuid());
    }

    public void clearEventBus() {
        eventBus.clear();
    }

    public void togglePause() {
        paused = !paused;
        UIMenuPanel.SetActive(paused);
        Time.timeScale = paused ? 0.0f : 1.0f;
        //eventBus.publish(new IT5Event(paused ? EventType.PAUSE : EventType.RESUME));
    }

    public void updateHealthLabel(int health) {
        playerHealth.text = "" + health;
    }

    public void updateWeaponLabel(string weapon) {
        playerWeapon.text = weapon;
    }

    public void updateItemLabel(string item) {
        playerItem.text = item;
    }
}
