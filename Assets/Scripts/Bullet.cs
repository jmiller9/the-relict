﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage = 30;
    public float range = 100.0f;
    public bool friendly = true;
    float distanceTraveled = 0;
    Vector3 lastPosition;

    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        distanceTraveled += Vector3.Distance(transform.position, lastPosition);
        lastPosition = transform.position;
        if (distanceTraveled >= range) {
            Destroy(transform.gameObject);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        bool acutallyHit = true;
        Debug.Log("Bullet collision detected");
        Humanoid humanoid = collider.gameObject.GetComponent<Humanoid>();
        if (humanoid != null) {
            acutallyHit = humanoid.ReceiveDamage(damage, friendly);
            Debug.Log("Bullet Hit Humanoid, actuallyHit = " + acutallyHit);
        }
        Bullet otherBullet = collider.gameObject.GetComponent<Bullet>();
        if (otherBullet != null) {
            acutallyHit = false;
            Debug.Log("Bullet Hit another Bullet");
        }
        if (acutallyHit) {
            Debug.Log("Destroying Bullet at " + transform.position);
            Destroy(transform.gameObject);
        }
    }
}
