﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Humanoid : IT5Object
{
    void Attack(Vector3 v);
    void ReachedWaypoint(Waypoint waypoint);
    bool ReceiveDamage(int damage, bool friendly);
    void SetHealth(int health);
    int GetHealth();
    void Animating (float h, float v);
}
