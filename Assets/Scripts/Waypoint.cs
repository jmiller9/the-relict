﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WaypointBehavior
{
    Continue,
    Stop,
    WaitAndContinue,
    Sleep,
    Explode,
    Die
};

public class Waypoint : MonoBehaviour
{
    public WaypointBehavior behavior;
    private Dictionary<string, GameObject> contained = new Dictionary<string, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Init of waypoint " + this.gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    bool ReachedWaypoint(GameObject gameObject) {
        Humanoid humanoid;
        humanoid = gameObject.GetComponent<Humanoid>();
        if (humanoid != null) {
            humanoid.ReachedWaypoint(this);
            return true;
        }
        return false;
    }

    void OnTriggerEnter(Collider collider)
    {
        //ContactPoint contact = collision.contacts[0];
        //Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        //Vector3 pos = contact.point;
        if (!contained.ContainsKey(collider.gameObject.name)) {
            if (ReachedWaypoint(collider.gameObject)) {
                Debug.Log(collider.gameObject.name + " entered waypoint " + this.gameObject.name);
                contained.Add(collider.gameObject.name, collider.gameObject);
            }
        }
    }

    void OnTriggerExit(Collider collider) {
        if (contained.ContainsKey(collider.gameObject.name)) {
            Debug.Log(collider.gameObject.name + " exited waypoint " + this.gameObject.name);
            contained.Remove(collider.gameObject.name);
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }
}
